package model;

public class IntegrantNode {
	private String			name;
	private IntegrantNode	next;

	public IntegrantNode() {
	}

	public IntegrantNode(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IntegrantNode getNext() {
		return next;
	}

	public void setNext(IntegrantNode next) {
		this.next = next;
	}
}
