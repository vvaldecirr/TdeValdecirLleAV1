package model;

public class SimpleLinkedList {
	private IntegrantNode fna; // first node address

	// initialize an list with one element
	public SimpleLinkedList() {
		this.fna = null;
	}

	// initialize an empty list
	public SimpleLinkedList(IntegrantNode fna) {
		this.setFna(fna);
	}

	public IntegrantNode getFna() {
		return fna;
	}

	public void setFna(IntegrantNode fna) {
		this.fna = fna;
	}

	// add node always at beginning of the list
	public void addOnList(IntegrantNode node) {
		node.setNext(this.fna);
		this.fna = node;
	}

	// answer true for empty list and false for not empty
	public boolean isEmpty() {
		if (this.fna == null)
			return true;
		else
			return false;
	}

	// Always verify the next node for return the node before 
	public IntegrantNode findByName(String name) {
		if (this.fna != null) { // checking if the list is empty

			if (this.fna.getName() == name) // if found item in the very first node
				return this.fna;
			
			if (this.fna.getNext() != null) { // if the list have more than one node
				IntegrantNode aux = this.fna;
				
				// iterate the list till find the item
				do {
					if (aux.getNext().getName() == name)
						return aux; // returning the node before the found item
					
					aux = aux.getNext();
				} while(aux.getNext() != null);
			}
			
			return null; // when not found item
			
		} else
			return null; // when the list is empty 
	}

	// Remove an item from list
	public boolean removeFromList(String name) {
		IntegrantNode aux = findByName(name);

		if (aux != null) { // if effectively have something to delete, point the before to next next
			if (aux == this.fna)
				this.fna = aux.getNext(); // in case of deletion of the first element
			else
				aux.setNext(aux.getNext().getNext()); // deletion when not the first elements
			
			return true;
		} else
			return false; // nothing to delete
	}
	
	// return a String with all element's value in indicated order
	public String showList(char order) { // A = for ascending, and 'any other' = for descending
		if (this.fna == null)
			return "Empty list";
		else {				
			String result = "";
			IntegrantNode aux = this.fna;
			
			if (order == 'a') { // ascending String binding
				while (aux != null) {
					result += aux.getName()+"-";
					
					aux = aux.getNext();
				}
				
				return result;
				
			} else { // descending String binding
				while (aux != null) {
					result = aux.getName()+"-"+result;

					aux = aux.getNext();
				}
				
				return result;
			}
		}
	}
}